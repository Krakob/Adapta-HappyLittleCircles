* Clone into `~/.local/share/themes/Adapta-HappyLittleCircles`. 

* Select the theme using `xfwm4-settings`.

* Do not make eye contact with the circles until they are properly fastened. 

![Active window using the theme](screenshot_active.png)

![Inactive window using the theme](screenshot_inactive.png)
